
<section class="comonSection contactSection2" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <h2 class="sectionTitle2">
                    <small>Say Hello!</small>
                    We would love to hear from you.
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <div class="conInfos2">
                    <p class="pfirst"><i class="fa fa-phone"></i>1.800.321.4567-8</p>
                    <p class="pMid"><i class="fa fa-envelope-o"></i>support@dotsquare.com</p>
                    <p class="plast"><i class="fa fa-map-marker"></i>Some street, New York, USA</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">
                    <form class="conForms conforms2" method="post" action="#" id="contactForm">
                        <div class="col-lg-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                            <input type="text" name="con_name" id="con_name" class="inputa required" placeholder="NAME:"/>
                            <input type="email" name="con_email" id="con_email" class="inputa required" placeholder="EMAIL:"/>
                            <input type="text" name="con_phone" id="con_phone" class="inputa required" placeholder="PHONE:"/>
                        </div>
                        <div class="col-lg-6 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                            <textarea class="texputa required" id="con_message" name="con_message" placeholder="MESSAGE:"></textarea>
                        </div>
                        <div class="col-lg-12 text-right wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                            <button type="submit" class="submita" id="con_submit">Send Message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>