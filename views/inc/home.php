<section class="banner2" id="home">
    <div class="mainSlider">
        <div class="tp-banner1">
            <ul>
                <li data-transition="cube" data-slotamount="7" data-masterspeed="500" >
                    <img src="../asset/images/slide/1.jpg" alt="ThemeWar Eovo" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="tp-caption sfb text-right homeOneSlider"
                         data-x="center"
                         data-hoffset="0"
                         data-y="360"
                         data-voffset="0"
                         data-speed="800"
                         data-start="1200"
                         data-easing="Power4.easeOut">
                        <div class="slid2s">
                            <h2>We Design Websites</h2>
                        </div>
                    </div>
                    <div class="tp-caption sfb text-right homeOneSlider"
                         data-x="center"
                         data-hoffset="0"
                         data-y="402"
                         data-voffset="0"
                         data-speed="1000"
                         data-start="1500"
                         data-easing="Power4.easeOut">
                        <div class="slid2s">
                            <h1>That Works</h1>
                        </div>
                    </div>
                </li>
                <li data-transition="slotzoom-horizontal" data-slotamount="7" data-masterspeed="500" >
                    <img src="../asset/images/slide/2.jpg" alt="ThemeWar Eovo" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="tp-caption sfb text-right homeOneSlider"
                         data-x="center"
                         data-hoffset="0"
                         data-y="360"
                         data-voffset="0"
                         data-speed="800"
                         data-start="1200"
                         data-easing="Power4.easeOut">
                        <div class="slid2s">
                            <h2>We Develop Websites</h2>
                        </div>
                    </div>
                    <div class="tp-caption sfb text-right homeOneSlider"
                         data-x="center"
                         data-hoffset="0"
                         data-y="402"
                         data-voffset="0"
                         data-speed="1000"
                         data-start="1500"
                         data-easing="Power4.easeOut">
                        <div class="slid2s">
                            <h1>That Nice</h1>
                        </div>
                    </div>
                </li>
                <li data-transition="curtain-1" data-slotamount="7" data-masterspeed="500" >
                    <img src="../asset/images/slide/3.jpg" alt="ThemeWar Eovo" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="tp-caption sfb text-right homeOneSlider"
                         data-x="center"
                         data-hoffset="0"
                         data-y="360"
                         data-voffset="0"
                         data-speed="800"
                         data-start="1200"
                         data-easing="Power4.easeOut">
                        <div class="slid2s">
                            <h2>We Create Idea</h2>
                        </div>
                    </div>
                    <div class="tp-caption sfb text-right homeOneSlider"
                         data-x="center"
                         data-hoffset="0"
                         data-y="402"
                         data-voffset="0"
                         data-speed="1000"
                         data-start="1500"
                         data-easing="Power4.easeOut">
                        <div class="slid2s">
                            <h1>It's Awesome</h1>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="sllearnMore text-center">
        <a href="#features" class="scrolls learnMoreButton2">Learn More</a>
    </div>
</section>
<section class="comonSection welcome2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <h2 class="sectionTitle2">
                    <small>Welcome to stellar agency</small>
                    We craft beautiful websites.
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-5 col-lg-offset-1 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <p class="welP">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer rutrum justo ac 
                    volutpat sodaleset malesuada fames ac ante ipsum primis in faucibus. Duis 
                    ligula enim, auctor quis dui id, elementum sagittis ligula. 
                </p>
            </div>
            <div class="col-lg-5 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="350ms">
                <p class="welP">
                    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per 
                    inceptos himenaeos. Pellentesque vel fermentum. Donec ullamcorper 
                    dui bibendum. Praesent velit elit, placerat at lorem sagittis, 
                    ullamcorper sem. 
                </p>
            </div>
        </div>
        <div class="row wel2Row">
            <div class="col-lg-12 text-center wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
                <a href="#" class="learnMoreButton2">
                    Learn more about us
                </a>
            </div>
        </div>
    </div>
</section>