<!DOCTYPE html>
<html lang="en">
    
<!-- Mirrored from nasfactor.com/themes/stellar/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Mar 2017 15:53:14 GMT -->
<head>
        <title>Stellar - Creative & Agency Responsive HTML5 Template</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Include All CSS -->
        <link rel="stylesheet" type="text/css" href="../asset/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/animate.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/font-awesome.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/fontello.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/essential_icon.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/prettyPhoto.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/animate.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/owl.carousel.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/owl.transitions.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/magnific-popup.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/settings.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/preset.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/blogpopup.css"/>
        <link rel="stylesheet" type="text/css" href="../asset/css/responsive.css"/>
        <!-- End Include All CSS -->

        <!-- Favicon Icon -->
        <link rel="icon"  type="image/png" href="images/favicons.png">
        <!-- Favicon Icon -->

        <!--[if lt IE 9]>
            <script src="js/html5shiv.js"></script>
            <script src="js/respond.min.js"></script>
        <![endif]--> 
    </head>
    <body>
        <!-- Loader -->
        <div class="preloader">
            <img src="images/spinningwheel.gif" alt=""/>
        </div>
        <!-- Loader -->
        <div id="page" class="animat">
            <header class="header home2">
                <div class="sideMenuArea verticalMenu">
                    <a href="#" class="closeBtn">&times;</a>
                    <div class="logo">
                        <a href="index-2.html">Stellar</a>
                    </div>
                    <nav class="mainMenu">
                        <ul>
                            <li class="active scroll has-child">
                                <a href="#home">Home</a>
                                <ul>
                                    <li><a href="index-2.html">Home Minimal</a></li>
                                    <li><a href="index2.html">Home Agency</a></li>
                                    <li><a href="index3.html">Home diagonal</a></li>
                                </ul>
                            </li>
                            <li class="scroll"><a href="#features">Features</a></li>
                            <li class="scroll has-child">
                                <a href="#portfolio">Portfolio</a>
                                <ul>
                                    <li><a href="singlefolio.html">Single Portfolio</a></li>
                                </ul>
                            </li>
                            <li class="scroll"><a href="#team">Team</a></li>
                            <li class="scroll has-child">
                                <a href="#blog">Blog</a>
                                <ul>
                                    <li><a href="blog.html">Blog Listing</a></li>
                                    <li><a href="singleblog.html">Single Blog</a></li>
                                </ul>
                            </li>
                            <li class="scroll"><a href="#contact">Contact</a></li>
                        </ul>
                    </nav>
                    <div class="clearfix"></div>
                    <div class="headerSocial">
                        <a class="fac" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="ins" href="#"><i class="fa fa-instagram"></i></a>
                        <a class="lin" href="#"><i class="fa fa-linkedin"></i></a>
                        <a class="goo" href="#"><i class="fa fa-google-plus"></i></a>
                        <a class="twi" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="dri" href="#"><i class="fa fa-dribbble"></i></a>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3">
                            <div class="logo">
                                <a href="index-2.html">Stellar</a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-sm-9 horizontalmenu">
                            <div class="menuButtons2 hidden-lg hidden-md hidden-sm hidden">
                                <a href="#">
                                    <img src="images/nav.png" alt=""/>
                                    <img class="hovImg2" src="images/navActive.png" alt=""/>
                                </a>
                            </div>
                            <nav class="mainMenu">
                                <ul>
                                    <li class="active scroll has-child">
                                        <a href="#home">Home</a>
                                        <ul>
                                            <li><a href="index-2.html">Home Minimal</a></li>
                                            <li><a href="index2.html">Home Agency</a></li>
                                            <li><a href="index3.html">Home diagonal</a></li>
                                        </ul>
                                    </li>
                                    <li class="scroll"><a href="#features">Features</a></li>
                                    <li class="scroll has-child">
                                        <a href="#portfolio">Portfolio</a>
                                        <ul>
                                            <li><a href="singlefolio.html">Single Portfolio</a></li>
                                        </ul>
                                    </li>
                                    <li class="scroll"><a href="#team">Team</a></li>
                                    <li class="scroll has-child">
                                        <a href="#blog">Blog</a>
                                        <ul>
                                            <li><a href="blog.html">Blog Listing</a></li>
                                            <li><a href="singleblog.html">Single Blog</a></li>
                                        </ul>
                                    </li>
                                    <li class="scroll"><a href="#contact">Contact</a></li>
                                </ul>
                                <div class="menuButtons">
                                    <a href="#">
                                        <img src="images/nav.png" alt=""/>
                                        <img class="hovImg" src="images/navActive.png" alt=""/>
                                    </a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>

            </header>